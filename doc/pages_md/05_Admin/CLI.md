title: CLI commands
navi_name: CLI

# Alambic Command Line Interface

Alambic offers a command line interface for some of its features:

* `init` Initialisation of the database and instance.
* `backup` Backups

## Using the Alambic CLI:

On the file system, go to the `mojo` directory and issue the following commands:
```
bin/alambic alambic <command> <options>
```
