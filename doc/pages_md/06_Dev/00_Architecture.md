title: Architecture Development
navi_name: Architecture


## Developing Alambic

Mojolicious has a special feature to help developers write code. By executing the `morbo` command instead of hypnotoad, the application is restarted every time a file is modified. Go to the mojo directory and open a terminal (or command) and type in:

    morbo bin/alambic

This will start the Mojolicious 'development' server.

It is still required to run the minion job management system in order to run projects.
